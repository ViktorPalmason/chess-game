#include <glad/gl.h>

const GLchar* chess_vs_source = R"(
#version 430 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec4 col;
layout(location = 2) in vec2 texCoords;

uniform mat4 mvp;
out vec4 vs_col;
out vec2 vs_tex;

void main()
{
	gl_Position = mvp* vec4(pos,1.0);
	vs_col = col;
	vs_tex = texCoords;
}
)";

const GLchar* chess_fs_source = R"(
#version 430 core

in vec4 vs_col;
in vec2 vs_tex;
out vec4 colour;

layout(binding = 0) uniform sampler2D boardTexture;
uniform bool addTexture;


void main()
{
	if(addTexture)
		colour = mix(vs_col, texture(boardTexture, vs_tex) ,0.7);
	else
		colour = vs_col;
}
)";

const GLchar* cube_vs_source = R"(
#version 430 core

layout(location = 3) in vec3 pos;
layout(location = 4) in vec4 col;
layout(location = 5) in vec3 instancePos;
layout(location = 6) in vec4 instanceCol;

uniform mat4 mvp;
uniform float cubeID = 0;
uniform float chosenCubeID = 0;
uniform bool selecting = true;
uniform bool tileChosen = false;
out vec3 vs_pos;
out vec4 vs_col;

void main()
{
	gl_Position = mvp* vec4((pos+instancePos),1.0);
	if (gl_InstanceID == chosenCubeID && tileChosen)
		vs_col = vec4(1, 1, 0, 1);
	else if (gl_InstanceID == cubeID && selecting)
		vs_col = vec4(0,1,0,1);
	else
		vs_col = col-instanceCol;
	vs_pos = pos;
}
)";

const GLchar* cube_fs_source = R"(
#version 430 core

in vec4 vs_col;
in vec3 vs_pos;
out vec4 colour;

layout (binding=1) uniform samplerCube cubeTex;
uniform bool addTexture;

void main()
{
	if(addTexture)
		colour = mix(vs_col, texture(cubeTex, vs_pos), 0.7);
	else	
		colour = vs_col;
}
)";