#ifndef GLFWAPPLICATION_H_
#define GLFWAPPLICATION_H_

#include <glad/gl.h>
#include <GLFW/glfw3.h>
#include <tclap/CmdLine.h>
#include <string>

class GLFWApplication
{
public:
    /// Window width
    GLint wWidth;
    /// Window height
    GLint wHeight;
    /// Application window
    GLFWwindow* window;
    /// Application title
    std::string Title;
    /// Application version number
    std::string Version;

    // A pointer that references the app, i.e. a pointer to itself
    static GLFWApplication* app;

    GLFWApplication(const std::string& name, const std::string& version);
    ~GLFWApplication();

    virtual unsigned int ParseArguments(int argc, char** argv);
    virtual unsigned int Init();
    virtual unsigned int Run() = 0;

    virtual void onKey(int key, int action);
    virtual void onResize(int width, int height);

    static void GLAPIENTRY GLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam);

    static void GLFWErrorCallback(GLint code, const GLchar* description);
    static void GLFWSizeCallback(GLFWwindow* window, int width, int height);
    static void GLFWKeyCallBack(GLFWwindow* window, int key, int scancode, int action, int mods);
};

#endif