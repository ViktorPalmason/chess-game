#include "GLFWApplication.h"

GLFWApplication* GLFWApplication::app;

GLFWApplication::GLFWApplication(const std::string& name, const std::string& version)
{
    Title = name;
    Version = version;
    wWidth = 800;
    wHeight = 600;
    app = this;
};
GLFWApplication:: ~GLFWApplication()
{
    glfwDestroyWindow(window);
    glfwTerminate();
};

unsigned int GLFWApplication::ParseArguments(int argc, char** argv)
{
    return EXIT_SUCCESS;
}

void GLAPIENTRY GLFWApplication::GLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam)
{
    fprintf(stderr, "GL DEBUG CALLBACK: %s type = 0x%xm severity 0x%x\nMessage: %s.\n", (type == GL_DEBUG_TYPE_ERROR ? " **GL ERROR** " : ""), type, severity, message);
}

void GLFWApplication::GLFWErrorCallback(GLint code, const GLchar* description)
{
    fprintf(stderr, "GLFW Error!\nCode: 0x%0x\nDescription: %s\n", code, description);
}

void GLFWApplication::GLFWSizeCallback(GLFWwindow* window, int width, int height)
{
    app->onResize(width, height);
}

void GLFWApplication::GLFWKeyCallBack(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    app->onKey(key, action);
}

unsigned int GLFWApplication::Init()
{
    glfwSetErrorCallback(GLFWErrorCallback);

    if (!glfwInit())
    {
        fprintf(stderr, "Failed to initialize GLFW!\n");
        return EXIT_FAILURE;
    }

    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

    window = glfwCreateWindow(wWidth, wHeight, Title.c_str(), nullptr, nullptr);
    if (!window)
    {
        fprintf(stderr, "Failed to create a window and an OpenGL context!\n");
        glfwTerminate();
        return EXIT_FAILURE;
    }

    glfwMakeContextCurrent(window);
    glfwSetWindowSizeCallback(window, GLFWSizeCallback);
    glfwSetKeyCallback(window, GLFWKeyCallBack);
    //glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

    if (!gladLoadGL(glfwGetProcAddress))
    {
        fprintf(stderr, "Failed to initialize GLAD2!\n");
        glfwDestroyWindow(window);
        glfwTerminate();
        return EXIT_FAILURE;
    }
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(GLDebugCallback, 0);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    glViewport(0, 0, wWidth, wHeight);

    fprintf(stdout, "Renderer: %s\n", glGetString(GL_RENDERER));
    fprintf(stdout, "vendor: %s\n", glGetString(GL_VENDOR));
    fprintf(stdout, "OpenGL Version: %s\n", glGetString(GL_VERSION));
    fprintf(stdout, "Initialization done\n");

    return EXIT_SUCCESS;
}

void GLFWApplication::onKey(int key, int action) 
{

}

void GLFWApplication::onResize(int width, int height) 
{
    wWidth = width;
    wHeight = height;
}
