#include <array>
#include <vector>

namespace GeometricTools
{
    constexpr std::array<float, 3*6> UnitTriangle2D = 
    {
        -0.5, -0.5f, 1.0f, 0.f, 0.f, 1.0f,
        0.5f, -0.5f, 1.0f, 0.f, 0.f, 1.0f,
        0.0f, 0.5f, 1.0f, 0.f, 0.f, 1.0f
    };

    constexpr std::array<float, 6*6> UnitSquare2D =
    {
        -0.5f, -0.5f, 0.f, 1.0f, 0.f, 1.0f,
        0.5f, -0.5f, 0.f, 1.0f, 0.f, 1.0f,
        -0.5, 0.5f, 0.f, 1.0f, 0.f, 1.0f,

        0.5f, -0.5f, 0.f, 1.0f, 0.f, 1.0f,
        0.5f, 0.5f, 0.f, 1.0f, 0.f, 1.0f,
        -0.5f, 0.5f, 0.f, 1.0f, 0.f, 1.0f,
    };

    constexpr std::array<float, 8 * 7> UnitCube3D =
    {
        // Front Square
       -0.5f, -0.5f, 0.5f, 1.f, 1.0f, 1.f, 1.0f,   // 0
        0.5f, -0.5f, 0.5f, 1.f, 1.0f, 1.f, 1.0f,   // 1
       -0.5f,  0.5f, 0.5f, 1.f, 1.0f, 1.f, 1.0f,   // 2
        0.5f,  0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 1.0f, // 3
        // Back Square                             
       -0.5f, -0.5f, -0.5f, 1.f, 1.0f, 1.f, 1.0f,  // 4
        0.5f, -0.5f, -0.5f, 1.f, 1.0f, 1.f, 1.0f,  // 5
       -0.5f,  0.5f, -0.5f, 1.f, 1.0f, 1.f, 1.0f,  // 6
        0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f,// 7
    };

    constexpr std::array<float, 8 * 7> SmallUnitCube3D =
    {
        // Front Square
       -0.025f, -0.025f,  0.025f, 1.f, 1.0f, 1.f, 1.0f,   // 0
        0.025f, -0.025f,  0.025f, 1.f, 1.0f, 1.f, 1.0f,   // 1
       -0.025f,  0.025f,  0.025f, 1.f, 1.0f, 1.f, 1.0f,   // 2
        0.025f,  0.025f,  0.025f, 1.0f, 1.0f, 1.0f, 1.0f, // 3
        // Back Squre 
       -0.025f, -0.025f, -0.025f, 1.f, 1.0f, 1.f, 1.0f,  // 4
        0.025f, -0.025f, -0.025f, 1.f, 1.0f, 1.f, 1.0f,  // 5
       -0.025f,  0.025f, -0.025f, 1.f, 1.0f, 1.f, 1.0f,  // 6
        0.025f,  0.025f, -0.025f, 1.0f, 1.0f, 1.0f, 1.0f,// 7
    };

    constexpr std::array<unsigned int, 3 * 12> UnitCube3DTopology =
    {
        0, 1, 2,
        1, 3, 2,

        1, 5, 3,
        5, 7, 3,

        5, 4, 7,
        4, 6, 7,

        4, 0, 6,
        0, 2, 6,

        2, 3, 6,
        3, 7, 6,

        4, 5, 0,
        5, 1, 0
    };

    template <int  x=1, int y=1>
    constexpr std::vector<float> UnitGridGeometry2D()
    {
        std::array<float, 7 * 4> grid;
        std::vector<float> gridVec;
        float posXCoord = 1.f / x;
        float posYCoord = 1.f / y;

        for (int i = 0; i < y; i++)
        {
            for (int j = 0; j < x; j++)
            {
                float colour = (float)((i + j) % 2);
                float xCoord = (posXCoord * j);
                float yCoord = (posYCoord * i);
                grid =
                {
                   -0.5f + xCoord,                 -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f,
                   (-0.5f + posXCoord) + xCoord,   -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f,
                   -0.5f + xCoord,                 (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f,
                   (-0.5f + posXCoord) + xCoord,   (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f,
                };
                gridVec.insert(gridVec.end(), begin(grid), end(grid));
            }
        }
        return gridVec;
    };

    template <int x=1, int y=1>
    constexpr std::vector<unsigned int> UnitGridTopologyTriangle()
    {
        std::vector<unsigned int> gridInd;
        for (int i = 0; i < y * x; i++)
        {
            gridInd.push_back(0 + (i * 4));
            gridInd.push_back(1 + (i * 4));
            gridInd.push_back(2 + (i * 4));

            gridInd.push_back(1 + (i * 4));
            gridInd.push_back(3 + (i * 4));
            gridInd.push_back(2 + (i * 4));
        }

        return gridInd;
    };

    template <int  x = 1, int y = 1>
    constexpr std::vector<float> UnitGridGeometry2DWTcoords()
    {
        std::array<float, 9 * 4> grid;
        std::vector<float> gridVec;
        float posXCoord = 1.f / x;
        float posYCoord = 1.f / y;

        for (int i = 0; i < y; i++)
        {
            for (int j = 0; j < x; j++)
            {
                float colour = (float)((i + j) % 2);
                float xCoord = (posXCoord * j);
                float yCoord = (posYCoord * i);
                grid =
                {
                   -0.5f + xCoord,                 -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f, 0.0f, 0.0f,
                   (-0.5f + posXCoord) + xCoord,   -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f, 0.0f, 1.0f,
                   -0.5f + xCoord,                 (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f, 1.0f, 0.0f,
                   (-0.5f + posXCoord) + xCoord,   (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f, 1.0f, 1.0f
                };
                gridVec.insert(gridVec.end(), begin(grid), end(grid));
            }
        }
        return gridVec;
    };

    template <int x = 1, int y = 1>
    constexpr std::vector<float> UnitCube3DTeams()
    {
        std::vector<float> teams;
        std::array<float, 8 * 7> teamsdata;


        // The dimensions of a tile 
        float tileX = 1.0f / 8.0f;
        float tileY = 1.0f / 8.0f;

        // the dimensions of the unit cubes relative to the size of a single tile
        float cubeX = tileX * 0.4f;
        float cubeY = tileY * 0.4f;
        float cubeZ = ((tileX+tileY)/2) * 0.4f;

        // The starting coord of the units
        float startX = -0.5f + (tileX-cubeX)/2;
        float startY = -0.5f + (tileY-cubeY)/2;
        float startZ = -0.39f;

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                float colour = 1.0f;
                float xCoord = (tileX * j);
                float yCoord = (tileY * i);
                teamsdata =
                {//   X                          Y                       Z              R     G      B      A
                    startX + xCoord,          startY + yCoord,         startZ + cubeZ, 0.0f, 0.0f, colour, 1.0f,
                   (startX + cubeX) + xCoord, startY + yCoord,         startZ + cubeZ, 0.0f, 0.0f, colour, 1.0f,
                    startX + xCoord,         (startY + cubeY) + yCoord,startZ + cubeZ, 0.0f, 0.0f, colour, 1.0f,
                   (startX + cubeX) + xCoord,(startY + cubeY) + yCoord,startZ + cubeZ, 0.0f, 0.0f, colour, 1.0f,

                    startX + xCoord,          startY + yCoord,         startZ        , 0.0f, 0.0f, colour, 1.0f,
                   (startX + cubeX) + xCoord, startY + yCoord,         startZ        , 0.0f, 0.0f, colour, 1.0f,
                    startX + xCoord,         (startY + cubeY) + yCoord,startZ        , 0.0f, 0.0f, colour, 1.0f,
                   (startX + cubeX) + xCoord,(startY + cubeY) + yCoord,startZ        , 0.0f, 0.0f, colour, 1.0f,
                };
                teams.insert(teams.end(), begin(teamsdata), end(teamsdata));
            }
        }

        for (int i = 6; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                float colour = 1.0f;
                float xCoord = (tileX * j);
                float yCoord = (tileY * i);
                teamsdata =
                {//   X                          Y                       Z              R     G      B      A
                    startX + xCoord,          startY + yCoord,         startZ + cubeZ, colour, 0.0f, 0.0f, 1.0f,
                   (startX + cubeX) + xCoord, startY + yCoord,         startZ + cubeZ, colour, 0.0f, 0.0f, 1.0f,
                    startX + xCoord,         (startY + cubeY) + yCoord,startZ + cubeZ, colour, 0.0f, 0.0f, 1.0f,
                   (startX + cubeX) + xCoord,(startY + cubeY) + yCoord,startZ + cubeZ, colour, 0.0f, 0.0f, 1.0f,

                    startX + xCoord,          startY + yCoord,         startZ        , colour, 0.0f, 0.0f, 1.0f,
                   (startX + cubeX) + xCoord, startY + yCoord,         startZ        , colour, 0.0f, 0.0f, 1.0f,
                    startX + xCoord,         (startY + cubeY) + yCoord,startZ        , colour, 0.0f, 0.0f, 1.0f,
                   (startX + cubeX) + xCoord,(startY + cubeY) + yCoord,startZ        , colour, 0.0f, 0.0f, 1.0f,
                };
                teams.insert(teams.end(), begin(teamsdata), end(teamsdata));
            }
        }

        return teams;
    };
    
    template <int x= 1, int y = 1>
    constexpr std::vector<float> UnitCube3DTeamsInstancePosition()
    {
        std::vector<float> teamsInstancePos;

        // unit offsets
        float startX = -0.5f + (1.0f / 16.0f); // + 0.0625
        float startY = -0.5f + (1.0f / 16.0f); // + 0.0625
        float startZ = -0.3675f;
        float tileSize = 1.0f / 8.0f; //0.125

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                teamsInstancePos.push_back(startX + (j * tileSize));
                teamsInstancePos.push_back(startY + (i * tileSize));
                teamsInstancePos.push_back(startZ);
            }
        }

        for (int i = 6; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                teamsInstancePos.push_back(startX + (j * tileSize));
                teamsInstancePos.push_back(startY + (i * tileSize));
                teamsInstancePos.push_back(startZ);
            }
        }

        return teamsInstancePos;
    }

    template <int x = 1, int y = 1>
    constexpr std::vector<unsigned int> UnitCube3DTeamsTopology()
    {
        std::vector<unsigned int> gridInd;
        for (int i = 0; i < 16*4; i++)
        {
            gridInd.push_back(0 + (i * 8));
            gridInd.push_back(1 + (i * 8));
            gridInd.push_back(2 + (i * 8));
                                       
            gridInd.push_back(1 + (i * 8));
            gridInd.push_back(3 + (i * 8));
            gridInd.push_back(2 + (i * 8));
                                       
            gridInd.push_back(1 + (i * 8));
            gridInd.push_back(5 + (i * 8));
            gridInd.push_back(3 + (i * 8));
                                       
            gridInd.push_back(5 + (i * 8));
            gridInd.push_back(7 + (i * 8));
            gridInd.push_back(3 + (i * 8));

            gridInd.push_back(5 + (i * 8));
            gridInd.push_back(4 + (i * 8));
            gridInd.push_back(7 + (i * 8));

            gridInd.push_back(4 + (i * 8));
            gridInd.push_back(6 + (i * 8));
            gridInd.push_back(7 + (i * 8));

            gridInd.push_back(4 + (i * 8));
            gridInd.push_back(0 + (i * 8));
            gridInd.push_back(6 + (i * 8));

            gridInd.push_back(0 + (i * 8));
            gridInd.push_back(2 + (i * 8));
            gridInd.push_back(6 + (i * 8));

            gridInd.push_back(2 + (i * 8));
            gridInd.push_back(3 + (i * 8));
            gridInd.push_back(6 + (i * 8));

            gridInd.push_back(3 + (i * 8));
            gridInd.push_back(7 + (i * 8));
            gridInd.push_back(6 + (i * 8));

            gridInd.push_back(4 + (i * 8));
            gridInd.push_back(5 + (i * 8));
            gridInd.push_back(0 + (i * 8));

            gridInd.push_back(5 + (i * 8));
            gridInd.push_back(1 + (i * 8));
            gridInd.push_back(0 + (i * 8));
        }

        return gridInd;
    };
}