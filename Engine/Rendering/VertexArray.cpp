#include "VertexArray.h"

VertexArray::VertexArray()
{
    //BindingIdx = 0;
    glCreateVertexArrays(1, &ID);
}

VertexArray::~VertexArray()
{
    this->Unbind();
    glDeleteVertexArrays(1, &ID);
}

void VertexArray::Bind()
{
    glBindVertexArray(ID);
}

void VertexArray::Unbind()
{
    glBindVertexArray(0);
}

void VertexArray::AddVertexBuffer(const std::shared_ptr<VertexBuffer>& vertexBuffer)
{
    // Define the layout of the attributes in the VAO
    for (auto&& attrib : vertexBuffer->Layout.GetAttributes())
    {
        glVertexArrayAttribBinding(ID,      // The ID of the VAO
            attrib.Location,                // The location of the attribute in the shader
            vertexBuffer->VertexBufferID);  // The Vertex buffer binding point to bind the attribute to.

        glVertexArrayAttribFormat(ID,       // The ID of the VAO
            attrib.Location,                // The index to the attribute whos format is being described 
            ShaderDataTypeComponentCount(attrib.Type),  // The number of components that make up the attribute
            ShaderDataTypeToOpenGLBaseType(attrib.Type),// The data type of the data that represents the attribute
            attrib.Normalized,                          // Indicate if the data should be normalized
            attrib.Offset);                             // The offset to the attribute data.

        glEnableVertexArrayAttrib(ID, attrib.Location);
    }
    // Bind the VBO to the VAO
    glVertexArrayVertexBuffer(ID,           // The ID of the VAO.
        vertexBuffer->VertexBufferID,       // The Vertex Buffer binding point to bind the buffer to.
        vertexBuffer->VertexBufferID,       // The ID of the VBO to bind.
        0,                                  // The offset to the first element in the buffer
        vertexBuffer->Layout.GetStride());  // The distance between the elements in the buffer.

    // Add the VBO to the list of VBO's associated with the VAO
    VertexBuffers.push_back(vertexBuffer);
}

void VertexArray::SetIndexBuffer(const std::shared_ptr<IndexBuffer>& indexBuffer)
{
    glVertexArrayElementBuffer(ID, indexBuffer->IndexBufferID);
    IdxBuffer = indexBuffer;
}