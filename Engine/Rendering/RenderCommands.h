#include <glad/gl.h>
#include <glm/vec4.hpp>
#include <VertexArray.h>

namespace RenderCommands
{
    inline void Clear(GLuint mode = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) { glClear(mode); }
    inline void ClearColor(glm::vec4 colour) {
        glClearBufferfv(GL_COLOR, 0, &colour[0]);
    }
    inline void ClearDepth(float depth) {
        glClearBufferfv(GL_DEPTH, 0, &depth);
    }
    inline void SetWireframeMode(GLenum face){
        glPolygonMode(face, GL_LINE);
    }
    inline void SetSolidMode(GLenum face)
    {
        glPolygonMode(face, GL_FILL);
    }
    inline void DrawIndex(const std::shared_ptr<VertexArray>& vao, GLenum primitive) 
    { 
        glDrawElements(primitive, vao->GetIndexBuffer()->GetCount(), GL_UNSIGNED_INT, nullptr);
    }
}