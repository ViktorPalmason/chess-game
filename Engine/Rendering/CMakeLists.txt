cmake_minimum_required(VERSION 3.15...3.25)

project(Rendering)

add_library(Rendering 
    VertexBuffer.cpp VertexBuffer.h 
    IndexBuffer.cpp IndexBuffer.h 
    ShaderDataTypes.h 
    BufferLayout.h
    VertexArray.h VertexArray.cpp
    Program.cpp Program.h
    RenderCommands.h
    TextureManager.h TextureManager.cpp
    Camera.h
    OrthographicCamera.h OrthographicCamera.cpp
    PerspectiveCamera.h PerspectiveCamera.cpp)
add_library(Engine::Rendering ALIAS Rendering)
target_include_directories(Rendering PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(Rendering PUBLIC glad glm stb)
target_compile_features(Rendering PUBLIC cxx_std_17)

target_compile_definitions(${PROJECT_NAME} PRIVATE STB_IMAGE_IMPLEMENTATION)
taRget_compile_definitions(${PROJECT_NAME} PRIVATE STB_IMAGE_STATIC)
