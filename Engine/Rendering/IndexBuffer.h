#ifndef INDEXBUFFER_H_
#define INDEXBUFFER_H_

#include <glad/gl.h>

class IndexBuffer
{
private:
    GLuint count;
public:
    IndexBuffer(GLuint* indices, GLsizei count);
    ~IndexBuffer();

    GLuint GetCount() const { return count; }

    GLuint IndexBufferID;
};

#endif