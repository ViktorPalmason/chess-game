#ifndef VERTEXARRAY_H_
#define VERTEXARRAY_H_

#include <glad/gl.h>
#include <VertexBuffer.h>
#include <IndexBuffer.h>
#include <BufferLayout.h>
#include "ShaderDataTypes.h"
#include <vector>
#include <memory>

class VertexArray
{
public:
    GLuint ID;
    std::vector<std::shared_ptr<VertexBuffer>> VertexBuffers;
    std::shared_ptr<IndexBuffer> IdxBuffer;

    VertexArray();
    ~VertexArray();

    void Bind();
    void Unbind();

    void AddVertexBuffer(const std::shared_ptr<VertexBuffer> &vertexBuffer);
    void SetIndexBuffer(const std::shared_ptr<IndexBuffer> &indexBuffer);

    const std::shared_ptr<IndexBuffer> &GetIndexBuffer() const { return IdxBuffer; }
    
    const std::vector<std::shared_ptr<VertexBuffer>> &GetVertexBuffers() const { return VertexBuffers; }
};
#endif