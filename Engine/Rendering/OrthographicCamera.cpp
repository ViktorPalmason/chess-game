#include <OrthographicCamera.h>


OrthographicCamera::OrthographicCamera(const Frustrum& frustrum,
    const glm::vec3& position, float rotation)
{
    CameraFrustrum = frustrum;
    Position = position;
    Rotation = rotation;

    ProjectionMatrix = glm::ortho(CameraFrustrum.left, CameraFrustrum.right, CameraFrustrum.bottom, CameraFrustrum.top, CameraFrustrum.near, CameraFrustrum.far);
    ViewMatrix = glm::lookAt(Position, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

    ViewProjectionMatrix = ProjectionMatrix * ViewMatrix;
}

void OrthographicCamera::RecalculateMatrix()
{
    ProjectionMatrix = glm::ortho(CameraFrustrum.left, CameraFrustrum.right, CameraFrustrum.bottom, CameraFrustrum.top, CameraFrustrum.near, CameraFrustrum.far);
    ViewMatrix = glm::lookAt(Position, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
    //glm::mat4 rot = glm::rotate(glm::mat4(1), glm::radians(Rotation), glm::vec3(0, 0, 1));

    ViewProjectionMatrix = ProjectionMatrix * ViewMatrix;
}

