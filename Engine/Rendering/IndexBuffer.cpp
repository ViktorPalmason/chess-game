#include <IndexBuffer.h>

IndexBuffer::IndexBuffer(GLuint* indices, GLsizei vertCount)
{
    count = vertCount;
    glCreateBuffers(1, &IndexBufferID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBufferID);
    glNamedBufferData(IndexBufferID, sizeof(indices) * count, indices, GL_DYNAMIC_DRAW);
}

IndexBuffer::~IndexBuffer()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &IndexBufferID);
}