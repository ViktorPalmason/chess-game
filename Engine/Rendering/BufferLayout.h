#ifndef BUFFERLAYOUT_H_
#define BUFFERLAYOUT_H_

#include "ShaderDataTypes.h"
#include <glad/gl.h>
#include <string>
#include <vector>

struct BufferAttribute
{
    BufferAttribute(ShaderDataType type, const std::string name, GLuint location, GLboolean normalized = false) 
        : Name(name), Location(location), Type(type), Size(ShaderDataTypeSize(type)), Offset(0), Normalized(normalized) 
    {
    }
    std::string Name;
    GLuint Location;
    ShaderDataType Type;
    GLuint Size;
    GLuint Offset;
    GLboolean Normalized;
};

class BufferLayout  
{
private:
    std::vector<BufferAttribute> Attributes;
    GLsizei Stride;

    void CalculateOffsetAndStride()
    {
        GLsizei offset = 0;
        this->Stride = 0;
        for (auto &attribute : Attributes)
        {
            attribute.Offset = offset;
            offset += attribute.Size;
            this->Stride += attribute.Size;
            //std::fprintf(stdout, "Name: %s Location: %i, Offset: %i, Size: %i\n", attribute.Name.c_str(), attribute.Location, attribute.Offset, attribute.Size);
        }        
        //std::fprintf(stdout, "Layout Stride %i\n", this->Stride);
    }
public:
    BufferLayout() {};
    ~BufferLayout() {};
    BufferLayout(const std::initializer_list<BufferAttribute> &attributes) : Attributes(attributes) {
        this->CalculateOffsetAndStride();
    }

    inline const std::vector<BufferAttribute> &GetAttributes() const { return this->Attributes; }
    inline GLsizei GetStride() const { return this->Stride; }

    std::vector<BufferAttribute>::iterator begin() { return this->Attributes.begin();}
    std::vector<BufferAttribute>::iterator end() { return this->Attributes.end(); }
    std::vector<BufferAttribute>::const_iterator begin() const { return this->Attributes.begin();}
    std::vector<BufferAttribute>::const_iterator end() const { return this->Attributes.end();}
};

#endif