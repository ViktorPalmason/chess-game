#include "chessApplication.h"
#include <shaders.h>


ChessApplication::ChessApplication(const std::string& name, const std::string& version) : GLFWApplication(name, version)
{
    camera.SetFrustrum({45.0f, float(wWidth), float(wHeight), 0.1f, 100.0f });
    camera.SetPosition(glm::vec3(0.0, 0.5f, 1.5f));
    camera.SetLookAt(glm::vec3(0.0f));

    tileSize = sizeof(float) * 9.0f * 4.0f;
    vertSize = sizeof(float) * 9.0f;
    colOffset = sizeof(float) * 3.0f;

    // adding Instanced ID of the cubes onto the board, they are placed on their starting positions or tiles.
    int cubeID = 0;
    for (int j = 0; j < 16; j++)
    {
        unitLocations[j] = cubeID++;
    }
    for (int i = 48; i < 64; i++)
    {
        unitLocations[i] = cubeID++;
    }
}

ChessApplication::~ChessApplication()
{
}

unsigned int ChessApplication::ParseArguments(int argc, char** argv)
{
    std::string msg;
    try
    {
        TCLAP::CmdLine cmd("Lab commands", ' ', Version);
        TCLAP::ValueArg<int> cmdWidth("w", "width=", "Set the width of the window", false, 800, "int");
        TCLAP::ValueArg<int> cmdHeight("g", "height=", "Set the height of the window", false, 800, "int");

        cmd.add(cmdWidth);
        cmd.add(cmdHeight);

        cmd.parse(argc, argv);

        wWidth = cmdWidth.getValue();
        wHeight = cmdHeight.getValue();

    }
    catch (TCLAP::ArgException& e)
    {
        fprintf(stderr, "Command Line error: %s for arg %s\n", e.error().c_str(), e.argId().c_str());
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

unsigned int ChessApplication::Run()
{
    auto chessboard = GeometricTools::UnitGridGeometry2DWTcoords<8, 8>();
    vboChessboard = std::make_shared<VertexBuffer>(chessboard.data(), sizeof(chessboard[0]) * chessboard.size());
    auto layout = BufferLayout({
        {ShaderDataType::Float3, "position", 0},
        {ShaderDataType::Float4, "colour", 1},
        {ShaderDataType::Float2, "texCoords", 2}
        });

    vboChessboard->SetLayout(layout);

    //auto cube = GeometricTools::UnitCube3DTeams();
    auto cube = GeometricTools::SmallUnitCube3D;
    vboCube = std::make_shared<VertexBuffer>(cube.data(), sizeof(cube.data())*cube.size());
    auto cubeLayout = BufferLayout({
        {ShaderDataType::Float3, "position", 3},
        {ShaderDataType::Float4, "colour", 4}
        });
    vboCube->SetLayout(cubeLayout);

    // Get the sizes and offset into the data to alter the colour attributes of the vertices when a tile is selected.
    auto attr = layout.GetAttributes();
    tileSize = layout.GetStride() * 4;
    vertSize = layout.GetStride();
    colOffset = layout.GetAttributes()[1].Offset;

    auto indices = GeometricTools::UnitGridTopologyTriangle<8, 8>();
    auto ibo = std::make_shared<IndexBuffer>(indices.data(), indices.size());

    //auto cubeIndices = GeometricTools::UnitCube3DTeamsTopology();
    auto cubeIndices = GeometricTools::UnitCube3DTopology;
    auto cubeIbo = std::make_shared<IndexBuffer>(cubeIndices.data(), cubeIndices.size());

    auto vao = std::make_shared<VertexArray>();
    vao->Bind();
    vao->AddVertexBuffer(vboChessboard);
    vao->AddVertexBuffer(vboCube);
    vao->SetIndexBuffer(ibo);

    TextureManager* man = TextureManager::GetInstance();
    bool success = man->LoadTexture2DRGBA("board", std::string(TEXTURES_DIR) + "/floor_texture.png", 0);
    if (!success)
        return EXIT_FAILURE;
    success = man->LoadCubeMapRGBA("cube", std::string(TEXTURES_DIR) + "/cube_texture.png", 1);
    if (!success)
        return EXIT_FAILURE;

    chessProgram = std::make_shared<Program>();
    if (!chessProgram->AddShader(GL_VERTEX_SHADER, chess_vs_source))
        return EXIT_FAILURE;
    if (!chessProgram->AddShader(GL_FRAGMENT_SHADER, chess_fs_source))
        return EXIT_FAILURE;
    if (!chessProgram->Link())
        return EXIT_FAILURE;
    chessProgram->Bind();

    cubeProgram = std::make_shared<Program>();
    if (!cubeProgram->AddShader(GL_VERTEX_SHADER, cube_vs_source))
        return EXIT_FAILURE;
    if (!cubeProgram->AddShader(GL_FRAGMENT_SHADER, cube_fs_source))
        return EXIT_FAILURE;
    if (!cubeProgram->Link())
        return EXIT_FAILURE;

    glm::mat4 iden = glm::mat4(1);
    glm::mat4 chessTrans = glm::translate(iden, glm::vec3(0, 0, 0));
    glm::mat4 chessRot = glm::rotate(iden, glm::radians(-90.0f), glm::vec3(1, 0, 0));
    glm::mat4 chessScale = glm::scale(iden, glm::vec3(1.f));
    glm::mat4 chessModel = chessTrans * chessRot * chessScale;
    glm::mat4 mvp = camera.GetViewProjectionMatrix() * chessModel;
    chessProgram->UploadUniformMat4("mvp", mvp);

    glm::mat4 cubeTrans = glm::translate(iden, glm::vec3(0, 0.4, 0));
    glm::mat4 cubeRot = glm::rotate(iden, glm::radians(-90.0f), glm::vec3(1, 0, 0));
    glm::mat4 cubeScale = glm::scale(iden, glm::vec3(1.f));
    cubeModel = cubeTrans * cubeRot * cubeScale;
    glm::mat4 cubeMvp = camera.GetProjectionMatrix() * camera.GetViewMatrix() * cubeModel;
    cubeProgram->Bind();
    cubeProgram->UploadUniformMat4("mvp", cubeMvp);

    // This sets the first tile at (0,0) in board coordinates selected.
    for (int i = 0; i < 4; i++)
    {
        vboChessboard->BufferSubData((vertSize *i) + colOffset, sizeof(selCol), &selCol);
    }

    glEnable(GL_DEPTH_TEST);

    //glEnable(GL_CULL_FACE);
    //glFrontFace(GL_CCW);

    auto instancePos = GeometricTools::UnitCube3DTeamsInstancePosition();

    vboInstancePos = std::make_shared<VertexBuffer>(instancePos.data(), sizeof(instancePos.data())*instancePos.size());
    auto instancePosLayout = BufferLayout({
        {ShaderDataType::Float3, "instancPos", 5}
        });
    vboInstancePos->SetLayout(instancePosLayout);
    vao->AddVertexBuffer(vboInstancePos);

    GLfloat instanceCol[2 * 4] = {
        1.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 1.0f, 0.0f
    };

    auto vboInstanceCol = std::make_shared<VertexBuffer>(instanceCol, sizeof(instanceCol));
    auto instanceColLayout = BufferLayout({
        {ShaderDataType::Float4, "instancCol", 6}
        });
    vboInstanceCol->SetLayout(instanceColLayout);
    vao->AddVertexBuffer(vboInstanceCol);

    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 16);

    float time = 0;
    float lastFrame = 0;
    float rotationDegrees = 0;

    do
    {
        RenderCommands::ClearColor(glm::vec4(0.4, 0.4, 0.4, 1));
        RenderCommands::ClearDepth(1.0f);
        time = glfwGetTime();

        if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) 
        {
            glm::vec3 newPos;
            rotationDegrees -= (time - lastFrame);
            if (rotationDegrees <= -360.0f / 50.0f)
                rotationDegrees = 0;
            newPos.x = distanceFromOrigin*glm::sin(glm::radians(rotationDegrees * 50));
            newPos.y = camera.GetPosition().y;
            newPos.z = distanceFromOrigin*glm::cos(glm::radians(rotationDegrees * 50));
            glm::vec4 test;
            glm::mat4 testmat;

            camera.SetPosition(newPos);
        };

        if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS)
        {
            glm::vec3 newPos;
            rotationDegrees += (time - lastFrame);
            if (rotationDegrees >= 360.0f/50.0f)
                rotationDegrees = 0;
            newPos.x = distanceFromOrigin*glm::sin(glm::radians(rotationDegrees *50));
            newPos.y = camera.GetPosition().y;
            newPos.z = distanceFromOrigin*glm::cos(glm::radians(rotationDegrees *50));

            camera.SetPosition(newPos);
        };
        // Zoom In
        if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
        {
            glm::vec3 newPos;
            distanceFromOrigin *= 0.99f;
            newPos.x = camera.GetPosition().x * 0.99f;
            newPos.y = camera.GetPosition().y * 0.99f;
            newPos.z = camera.GetPosition().z * 0.99f;
            camera.SetPosition(newPos);

        }
        if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
        {
            glm::vec3 newPos;
            distanceFromOrigin *= 1.01f;
            newPos.x = camera.GetPosition().x * 1.01f;
            newPos.y = camera.GetPosition().y * 1.01f;
            newPos.z = camera.GetPosition().z * 1.01f;
            camera.SetPosition(newPos);
        }
        lastFrame = time;

        vao->SetIndexBuffer(ibo);
        chessProgram->Bind();
        glm::mat4 mvp = camera.GetViewProjectionMatrix() * chessModel;
        chessProgram->UploadUniformMat4("mvp", mvp);
        RenderCommands::DrawIndex(vao, GL_TRIANGLES);

        vao->SetIndexBuffer(cubeIbo);
        cubeProgram->Bind();
        cubeMvp = camera.GetViewProjectionMatrix() * cubeModel;
        cubeProgram->UploadUniformMat4("mvp", cubeMvp);
        //RenderCommands::DrawIndex(vao, GL_TRIANGLES);
        glDrawElementsInstanced(GL_TRIANGLES, vao->GetIndexBuffer()->GetCount(), GL_UNSIGNED_INT, 0, 32);
        glfwSwapBuffers(window);


        glfwPollEvents();
    } while (!glfwWindowShouldClose(window));    

	return EXIT_SUCCESS;
}

void ChessApplication::onKey(int key, int action)
{
    if (key == GLFW_KEY_Q && action == GLFW_RELEASE) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }

    if (key == GLFW_KEY_UP && action == GLFW_PRESS && (currentTileY < 7))
    {
        float colour = (currentTileX + currentTileY++) % 2;
        glm::vec4 tileCol = glm::vec4(colour);
        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (currentTile += 8) * tileSize;

        // If a unit(cube) is on the current tile being selected then turn it green and if not turn of
        // the selecting of a unit so that the previous selected tile turns back to its normal colour.
        if (unitLocations.count(currentTile)) {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", true);
            cubeProgram->UploadUniformFloat1("cubeID", unitLocations[currentTile]);
        }
        else
        {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", false);
        }

        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), &selCol);
        }
    }

    if (key == GLFW_KEY_DOWN && action == GLFW_PRESS && (currentTileY > 0))
    {
        float colour = (currentTileX + currentTileY--) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (currentTile -= 8) * tileSize;
        // If a unit(cube) is on the current tile being selected then turn it green and if not turn of
        // the selecting of a unit so that the previous selected tile turns back to its normal colour.
        if (unitLocations.count(currentTile)) {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", true);
            cubeProgram->UploadUniformFloat1("cubeID", unitLocations[currentTile]);
        }
        else
        {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", false);
        }

        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), &selCol);
        }
    }

    if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS && (currentTileX % 8 < 7))
    {
        float colour = (currentTileY + currentTileX++) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (++currentTile) * tileSize;

        // If a unit(cube) is on the current tile being selected then turn it green and if not turn of
        // the selecting of a unit so that the previous selected tile turns back to its normal colour.
        if (unitLocations.count(currentTile)) {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", true);
            cubeProgram->UploadUniformFloat1("cubeID", unitLocations[currentTile]);
        }
        else
        {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", false);
        }
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), &selCol);
        }
    }

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS && (currentTileX % 8 > 0))
    {
        float colour = (currentTileY + currentTileX--) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (--currentTile) * tileSize;
        // If a unit(cube) is on the current tile being selected then turn it green and if not turn of
        // the selecting of a unit so that the previous selected tile turns back to its normal colour.
        if (unitLocations.count(currentTile)) {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", true);
            cubeProgram->UploadUniformFloat1("cubeID", unitLocations[currentTile]);
        }
        else
        {
            cubeProgram->Bind();
            cubeProgram->UploadUniformBool("selecting", false);
        }
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), &selCol);
        }
    }

    if (key == GLFW_KEY_T && action == GLFW_PRESS)
    {
        addTexture = !addTexture;
        chessProgram->Bind();
        chessProgram->UploadUniformBool("addTexture", addTexture);
        cubeProgram->Bind();
        cubeProgram->UploadUniformBool("addTexture", addTexture);
    }

    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {

        if (!movingTile && unitLocations.count(currentTile))
        {
            movingTile = true;
            cubeProgram->Bind();
            chosenCubeID = unitLocations[currentTile];
            chosenCubeTileLoc = currentTile;
            cubeProgram->UploadUniformFloat1("chosenCubeID", chosenCubeID);
            cubeProgram->UploadUniformBool("tileChosen", true);
        }
        else if (movingTile && unitLocations.count(currentTile))
        {
            movingTile = false;
            cubeProgram->UploadUniformBool("tileChosen", false);
        }
        else
        {
            movingTile = false;
            cubeProgram->UploadUniformBool("tileChosen", false);
            // The starting position of the units
            float startX = -0.5f + (1.0f / 16.0f); // + 0.0625
            float startY = -0.5f + (1.0f / 16.0f); // + 0.0625
            float tileSize = 1.0f / 8.0f; //0.125
            // I calculate a new position for cube based on the X and Y coordinate of the destination tile.
            float newPos[2] = { startX + (currentTileX * tileSize), startY + (currentTileY * tileSize) };
            // overide the position data of the unit in the vertex buffer that stores the position data for
            // each unit
            vboInstancePos->BufferSubData(sizeof(float) * 3 * chosenCubeID, sizeof(float) * 2, newPos);
            // Add the new location of the unit to the board map and remove the old location. 
            unitLocations[currentTile] = chosenCubeID;
            unitLocations.erase(chosenCubeTileLoc);
        }
    }
}

void ChessApplication::onResize(int width, int height)
{
    GLFWApplication::onResize(width, height);
    camera.SetFrustrum({ 45.0f, (float)wWidth, (float)wHeight, 0.1f, 100.0f });
    glViewport(0, 0, width, height);
}