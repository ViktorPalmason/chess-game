#include "chessApplication.h"

int main(int argc, char* argv[])
{
    ChessApplication app("ChessGame", "1.0");
    app.ParseArguments(argc, argv);
    app.Init();
    return app.Run();
}