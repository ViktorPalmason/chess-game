#include <GLFWApplication.h>

#include <VertexArray.h>
#include <VertexBuffer.h>
#include <Program.h>
#include <RenderCommands.h>
#include <GeometricTools.h>
#include <BufferLayout.h>
#include <PerspectiveCamera.h>
#include <TextureManager.h>

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

class ChessApplication : public GLFWApplication
{
private:
    std::shared_ptr<VertexBuffer> vboChessboard, vboCube, vboInstancePos;
    std::shared_ptr<Program> cubeProgram, chessProgram;

    // The Y coordinate of the current selecte tile of the chessboard.
    int currentTileY = 0;
    // The X coordinate of the current selecte tile of the chessboard.
    int currentTileX = 0;
    // The current selected tile of the chessboard, this is numbered from 0 to 63.
    int currentTile = 0;

    // These are variables used to locate and modify the colour values of the tiles.
    float tileSize;
    float vertSize;
    float colOffset;

    // This is the distance the camera has from the origin on the z-axis;
    float distanceFromOrigin = 1.5f;

    // A bool that tells if the textures should be added or not.
    bool addTexture = false;
    // A bool that tells that a tile has been chosen to be moved.
    bool movingTile = false;
    // The instance ID of the cube chosen to be moved.
    int chosenCubeID = 0;
    // The tile that the chosen cube to be moved in located on.
    int chosenCubeTileLoc = 0;
    // A map to store unit locations on the board. The key will be the Tile number and the value the Instance ID
    // of the unit(cube).
    std::map<int, int> unitLocations;

    // The color of a selected tile
    glm::vec4 selCol = glm::vec4(0, 1, 0, 1);
    //
    glm::mat4 cubeModel;

    // The perspective camera of the application
    PerspectiveCamera camera;

public:
    ChessApplication(const std::string& name, const std::string& version);
    ~ChessApplication();

    unsigned int ParseArguments(int argc, char** argv);
    unsigned int Run();  

    void onKey(int key, int action);
    void onResize(int width, int height);
};
